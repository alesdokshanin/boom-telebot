package boom

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"
)

const timezone = "Europe/Minsk"

type TimeInterval struct {
	Room       *Room
	StartTime  time.Time
	EndTime    time.Time
	IsReserved bool
}

type Room struct {
	Name string
	Url  string
}

var AllRooms = [...]Room{
	{"MAX", "https://studio.boomroom.by/repaby?rid=1"},
	{"GOLD", "https://studio.boomroom.by/repaby?rid=2"},
	{"VIP", "https://studio.boomroom.by/repaby?rid=3"},
	{"STAR", "https://studio.boomroom.by/repaby?rid=4"},
	{"JAM1", "https://studio.boomroom.by/repaby?rid=7"},
	{"JAM2", "https://studio.boomroom.by/repaby?rid=8"},
}

func GetIntervalsFromResponseBody(room *Room, responseBody *string) []*TimeInterval {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(*responseBody))
	if err != nil {
		log.Println("Failed to parse response body", err)
	}

	intervals := make([]*TimeInterval, 0)
	var date string

	doc.Find("table tr td:nth-child(2)").Each(func(i int, s *goquery.Selection) {
		if i == 0 {
			date = strings.Trim(s.Text(), " ")
		} else {
			timeFrameText := s.Text()
			if timeFrameText == "" {
				return // no night-long booking available
			}

			blockedChildrenSelection := s.ChildrenFiltered(".blocked")
			isReserved := len(blockedChildrenSelection.Nodes) > 0

			splitTimeFrame := strings.Split(timeFrameText, "-")
			startTime := strings.Trim(splitTimeFrame[0], " ")
			endTime := strings.Trim(splitTimeFrame[1], " ")

			item := createTimeIntervalFromTimeStrings(room, date, startTime, endTime, isReserved)
			intervals = append(intervals, &item)
		}
	})

	intervals = cleanOverlaps(intervals)
	sortIntervals(intervals)

	return intervals
}

func (ti *TimeInterval) String() string {
	timeLayout := "15:04"
	start := ti.StartTime.Format(timeLayout)
	end := ti.EndTime.Format(timeLayout)
	return fmt.Sprintf("%s  (%s - %s)  reserved: %t", ti.Room, start, end, ti.IsReserved)
}

func getTimeObjectFromStrings(dateString string, timeString string, isNextDay bool) time.Time {
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		log.Println("Cannot get location timezone")
	}
	now := time.Now().In(loc)

	currentYear := now.Year()
	dayDotMonth := dateString[len(dateString)-5:]
	dayMonth := strings.Split(dayDotMonth, ".")

	day, err := strconv.Atoi(dayMonth[0])
	if err != nil {
		log.Println("Could not get date from string", dateString)
	}

	month, err := strconv.Atoi(dayMonth[1])
	if err != nil {
		log.Println("Could not get date from string", dateString)
	}

	hour, err := strconv.Atoi(strings.Trim(strings.Split(timeString, ":")[0], " "))
	if err != nil {
		log.Println("Could not get time from string", timeString)
	}

	minute, err := strconv.Atoi(strings.Trim(strings.Split(timeString, ":")[1], " "))
	if err != nil {
		log.Println("Could not get time from string", timeString)
	}

	resultTime := time.Date(currentYear, time.Month(month), day, hour, minute, 0, 0, loc)
	if isNextDay {
		resultTime = resultTime.AddDate(0, 0, 1)
	}
	return resultTime
}

func createTimeIntervalFromTimeStrings(room *Room, date string, startTime string, endTime string,
	isReserved bool) TimeInterval {
	start := getTimeObjectFromStrings(date, startTime, false)

	// Edge case for a night-long booking (end date is next morning so we should add a day)
	var isNextDay bool
	if startTime[:2] == "23" && endTime[:2] == "6:" {
		isNextDay = true
	} else {
		isNextDay = false
	}

	end := getTimeObjectFromStrings(date, endTime, isNextDay)
	return TimeInterval{room, start, end, isReserved}
}

func cleanOverlaps(intervals []*TimeInterval) (cleaned []*TimeInterval) {
	cleaned = make([]*TimeInterval, 0)

	for _, interval := range intervals {
		shouldAppend := true

		for i := 0; i < len(cleaned); i++ {
			if interval.StartTime.Equal(cleaned[i].StartTime) {
				if interval.EndTime.After(cleaned[i].EndTime) {
					cleaned[i] = interval
					shouldAppend = false
				}

			} else if interval.StartTime.Before(cleaned[i].EndTime) {
				shouldAppend = false
				break
			}

		}
		if shouldAppend {
			cleaned = append(cleaned, interval)
		} else {
			continue
		}
	}

	return
}

func sortIntervals(intervals []*TimeInterval) {
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i].StartTime.Hour() < intervals[j].StartTime.Hour()
	})
}

func startTimeIsSuitable(startTime time.Time) (suitable bool) {
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		log.Fatalln("Failed to get timezone information")
	}

	now := time.Now().In(loc)

	beforeStartThreshold, _ := time.ParseDuration("30m")

	isInFuture := now.Before(startTime) && startTime.Sub(now).Seconds() > beforeStartThreshold.Seconds()
	isInDesiredTime := startTime.Hour() >= 19 && startTime.Hour() <= 21
	isNotWeekend := startTime.Weekday() <= 5 // excluding 6, 7 which stand for Sat and Sun
	isToday := startTime.YearDay() == now.YearDay()

	return isToday && isNotWeekend && isInFuture && isInDesiredTime
}

func timeIsSuitable(startTime time.Time, endTime time.Time) (suitable bool) {
	isNotTooShort := endTime.Sub(startTime).Hours() >= 2
	endsNotTooLate := endTime.Hour() <= 23
	return startTimeIsSuitable(startTime) && isNotTooShort && endsNotTooLate
}

func mergeIntervals(start, end *TimeInterval) (merged TimeInterval) {
	merged = TimeInterval{
		start.Room, start.StartTime, end.EndTime, start.IsReserved,
	}
	return
}

func SquashIntervals(intervals []*TimeInterval) (squashed []*TimeInterval) {
	sortIntervals(intervals)

	squashed = make([]*TimeInterval, 0)

	for _, cur := range intervals {
		extendPrev := false

		if len(squashed) > 0 {
			prev := squashed[len(squashed)-1]

			if cur.IsReserved == prev.IsReserved && prev.EndTime.Equal(cur.StartTime) {
				extendPrev = true
			}
		}

		if extendPrev {
			squashed[len(squashed)-1].EndTime = cur.EndTime
		} else {
			squashed = append(squashed,
				&TimeInterval{cur.Room, cur.StartTime, cur.EndTime, cur.IsReserved})
		}
	}

	return
}

func GetSuitableTimeIntervals(intervals []*TimeInterval) (suitable []*TimeInterval) {
	sortIntervals(intervals)

	var startInt, endInt *TimeInterval
	for i := 0; i < len(intervals); i++ {
		interval := intervals[i]

		if interval.IsReserved {
			if startInt != nil && endInt != nil {
				merged := mergeIntervals(startInt, endInt)
				suitable = append(suitable, &merged)
			}
			startInt, endInt = nil, nil
			continue
		}

		if startInt == nil {
			if startTimeIsSuitable(interval.StartTime) {
				startInt = interval
			}
			continue
		} else {
			if timeIsSuitable(startInt.StartTime, interval.EndTime) {
				endInt = interval
			}
		}
	}

	if startInt != nil && endInt != nil {
		merged := mergeIntervals(startInt, endInt)
		suitable = append(suitable, &merged)
	}

	return

}
