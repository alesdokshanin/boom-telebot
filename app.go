package main

import (
	"boom-telebot/boom"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

const adminId = 145729930
const timeLayout = "15:04"

var fetchedIntervals []*boom.TimeInterval = nil

func groupByRoomName(intervals []*boom.TimeInterval) map[string][]*boom.TimeInterval {
	m := make(map[string][]*boom.TimeInterval)

	for _, interval := range intervals {
		arr, ok := m[interval.Room.Name]

		if !ok {
			arr = make([]*boom.TimeInterval, 0)
		}

		m[interval.Room.Name] = append(arr, interval)
	}

	return m
}

type fetchResult struct {
	room         *boom.Room
	responseBody *string
}

func fetchUrl(room boom.Room, c chan *fetchResult, wg *sync.WaitGroup) {
	defer wg.Done()
	timeout := time.Duration(5 * time.Second)
	client := http.Client{Timeout: timeout}

	resp, err := client.Get(room.Url)
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		log.Println("Cannot fetch URL", err)
		return
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Failed to get response body", err)
		return
	}

	bodyString := string(bodyBytes)
	c <- &fetchResult{&room, &bodyString}
}

func fetchAllIntervalsForRooms(rooms []boom.Room) []*boom.TimeInterval {
	c := make(chan *fetchResult, len(rooms))
	wg := sync.WaitGroup{}

	for _, room := range rooms {
		wg.Add(1)
		go fetchUrl(room, c, &wg)
	}

	wg.Wait()
	close(c)

	allIntervals := make([]*boom.TimeInterval, 0)

	for fr := range c {
		intervals := boom.GetIntervalsFromResponseBody(fr.room, fr.responseBody)
		allIntervals = append(allIntervals, intervals...)
	}

	fmt.Println("Successfully fetched time intervals")

	return allIntervals
}

func main() {
	b, err := tb.NewBot(tb.Settings{
		Token:  os.Getenv("API_TOKEN"),
		Poller: &tb.LongPoller{Timeout: 15 * time.Second},
	})

	if err != nil {
		log.Fatalln("Cannot create bot", err)
		return
	}

	b.Handle("/health", func(m *tb.Message) {
		_, err := b.Send(m.Sender, "\xF0\x9F\x86\x97")

		if err != nil {
			log.Println(err)
		}
	})

	b.Handle("/all", func(m *tb.Message) {
		var msg string

		intervalsByRoom := groupByRoomName(fetchedIntervals)
		if len(intervalsByRoom) > 0 {
			var lines []string

			for _, room := range boom.AllRooms {
				intervals, ok := intervalsByRoom[room.Name]
				intervals = boom.SquashIntervals(intervals)

				if ok && len(intervals) > 0 {
					lines = append(lines, fmt.Sprintf("*%s*", room.Name))

					for _, i := range intervals {
						var reservation string
						if i.IsReserved {
							reservation = "x"
						} else {
							reservation = "  "
						}

						info := fmt.Sprintf("%s - %s [[%s]]", i.StartTime.Format(timeLayout),
							i.EndTime.Format(timeLayout), reservation)
						lines = append(lines, info)
					}
					lines = append(lines, "\n")
				}
			}

			msg = strings.Join(lines, "\n")

		} else {
			msg = "Could not fetch current timetable."
		}

		_, err = b.Send(m.Sender, msg, &tb.SendOptions{ParseMode: tb.ModeMarkdown})
		if err != nil {
			log.Println(err)
		}

	})

	b.Handle("/hot", func(m *tb.Message) {
		intervalsByRoom := groupByRoomName(boom.GetSuitableTimeIntervals(fetchedIntervals))
		dealsExist := len(intervalsByRoom) > 0

		var msg string
		var lines []string

		if dealsExist {
			for _, room := range boom.AllRooms {
				intervals, ok := intervalsByRoom[room.Name]
				if ok {
					lines = append(lines, fmt.Sprintf("*%s*", room.Name))

					for _, i := range intervals {
						info := fmt.Sprintf("%s - %s", i.StartTime.Format(timeLayout), i.EndTime.Format(timeLayout))
						lines = append(lines, info)
					}

					lines = append(lines, "\n")
				}
				msg = strings.Join(lines, "\n")
			}

		} else {
			msg = "*NO* hot deals available at the moment \xF0\x9F\x98\xA5"
		}

		_, err := b.Send(m.Sender, msg, &tb.SendOptions{
			ParseMode: tb.ModeMarkdown,
		})

		if err != nil {
			log.Println(err)
		}
	})

	recipient := tb.User{}
	recipient.ID = adminId

	_, err = b.Send(&recipient, "\xF0\x9F\xA4\x96  started \xF0\x9F\x9A\x80")
	if err != nil {
		log.Println(err)
	}

	fetchedIntervals = fetchAllIntervalsForRooms(boom.AllRooms[:])

	ticker := time.NewTicker(60 * time.Second)
	go func() {
		for range ticker.C {
			fetchedIntervals = fetchAllIntervalsForRooms(boom.AllRooms[:])
		}
	}()

	b.Start()

}
